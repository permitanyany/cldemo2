from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.datamodel.flow import (HeaderConstraints,PathConstraints)
from pybatfish.question import bfq
import re

NETWORK_NAME = "cumulus"
BASE_SNAPSHOT_NAME = "cumulus"
SNAPSHOT_PATH = "/home/cumulus/cldemo2/batfish"

load_questions()

print("[*] Initializing BASE_SNAPSHOT")
bf_set_network(NETWORK_NAME)
bf_init_snapshot(SNAPSHOT_PATH, name=BASE_SNAPSHOT_NAME, overwrite=True)

##############################################################################################################
bgpLeafStatus = bfq.bgpSessionStatus(nodes="/leaf/").answer().frame()

for index, row in bgpLeafStatus.iterrows():
	if(re.match(r'spine', str(row['Remote_Node'])) is not None):
		print("%s Has A Matching Spine BGP Neighbor" %str(row['Node']))
	else:
		raise Exception("%s Does NOT Have A Matching Spine BGP Neighbor" %str(row['Node']))

#############################################################################################################
bgpSpineStatus = bfq.bgpSessionStatus(nodes="/spine/").answer().frame()

for index, row in bgpSpineStatus.iterrows():
        if((re.match(r'65020', str(row['Local_AS'])) is not None)):
                print("%s is Using The Correct Spine AS" %str(row['Node']))
        else:
                raise Exception("%s is NOT Using The Correct Spine AS" %str(row['Node']))

##############################################################################################################

for index, row in bgpLeafStatus.iterrows():
	if ((int((str(row['Node']).split("f"))[1])) % 2) == 0:
		if(bgpLeafStatus.at[index-1,'Local_AS'] == row['Local_AS']):
			print("Leaf AS's Match")
		else:
			raise Exception("Leaf AS's DO NOT Match")
	else:
                if(bgpLeafStatus.at[index+1,'Local_AS'] == row['Local_AS']):
                        print("Leaf AS's Match")
                else:
                        raise Exception("Leaf AS's DO NOT Match")
